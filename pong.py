import pygame, random, math
from pygame.locals import *

window_width = 640
window_height = 480
screen = pygame.display.set_mode((window_width, window_height))
b_speed = 7
p_speed = 5
num_balls = 2
def distance(p1, p2):
    return math.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2)

class game:
    def __init__(self):
        pygame.init()
        # constants
        self.circles = []

    def clear(self, screen):
        screen.fill((0,0,0))


    def newGame(self, screen):
        global num_balls
        for i in range(num_balls):
            i = self.Circle()
            self.circles.append(i)
        arial = pygame.font.match_font('Verdana')
        font = pygame.font.Font(arial, 32)
        rect = pygame.Rect(290,230, 150, 55)
        screen.blit(font.render('3', True, (0, 0, 0), (255,255,255)), rect)
        pygame.display.update()
        pygame.display.flip()
        pygame.time.wait(1000)
        screen.blit(font.render('2', True, (0, 0, 0), (255,255,255)), rect)
        pygame.display.update()
        pygame.display.flip()
        pygame.time.wait(1000)
        screen.blit(font.render('1', True, (0, 0, 0), (255,255,255)), rect)
        pygame.display.update()
        pygame.display.flip()
        pygame.time.wait(1000)

    class Field:
        def __init__(self):
            v_start = 75
            h_start = 5

            self.pos = (h_start, v_start)
        def draw(self, screen):
            pygame.draw.rect(screen, (255, 255, 255), (self.pos[0], self.pos[1], 630, 400))

    class CompPaddle:
        def __init__(self):
            v_start = 200
            h_start = 615
            self.score = 0
            self.pos = (h_start, v_start)
            self.dir = 'bottom'

        def draw(self, screen):
            pygame.draw.rect(screen, (255, 0, 0), (self.pos[0], self.pos[1], 20, 100))
            arial = pygame.font.match_font('Verdana')
            font = pygame.font.Font(arial, 32)
            rect = pygame.Rect(475, 10, 150, 55)
            screen.blit(font.render('Score: ' + str(self.score), True, (255, 255, 255), (0,0,0)), rect)

        def move(self, screen, circles):
            global p_speed
            if len(circles) > 0:
                closest = []
                for i in circles:
                    dis = distance((self.pos[0], self.pos[1]), i.pos)
                    closest.append(dis)
                x_min  = min(closest)
                x = closest.index(x_min)
                closest_circle = circles[x]

                if closest_circle.pos[1] > self.pos[1] - 50:
                    self.dir = 'bottom'

                if closest_circle.pos[1] < self.pos[1] + 50:
                    self.dir = 'top'

                if self.dir == 'bottom' and self.pos[1] <= 475 - 100:
                    v_start = self.pos[1] + p_speed
                    h_start = self.pos[0]
                    self.pos = (h_start, v_start)
                elif self.dir == 'top' and self.pos[1] >= 74:
                    v_start = self.pos[1] - p_speed
                    h_start = self.pos[0]
                    self.pos = (h_start, v_start)

    class Paddle:
        def __init__(self):
            v_start = 200
            h_start = 5
            self.score = 0
            self.pos = (h_start, v_start)
        def draw(self, screen):
            pygame.draw.rect(screen, (0, 255, 0), (self.pos[0], self.pos[1], 20, 100))
            arial = pygame.font.match_font('Verdana')
            font = pygame.font.Font(arial, 32)
            rect = pygame.Rect(10, 10, 150, 55)
            screen.blit(font.render('Score: ' + str(self.score), True, (255, 255, 255), (0,0,0)), rect)
        def moveUp(self, screen):
            global p_speed
            if self.pos[1] > 76:
                v_start = self.pos[1] - p_speed
                h_start = self.pos[0]

                self.pos = (h_start, v_start)
        def moveDown(self, screen):
            global p_speed
            if self.pos[1] < 475 - 102:
                v_start = self.pos[1] + p_speed
                h_start = self.pos[0]
                self.pos = (h_start, v_start)
    class Circle:
        def __init__(self):
            vert = ['up','down']
            self.kill_flag = False
            horz = ['left','right']
            self.h_dir = random.choice(horz)
            self.v_dir = random.choice(vert)
            #v_list = [75,475]
            v_start = random.randint(75,475)
            #v_start = random.choice(v_list)
            h_list = [5, 635]
            h_start = random.choice(h_list)
            if h_start == 5:
                self.h_dir = 'right'
            else:
                self.h_dir = 'left'
            self.pos = (h_start, v_start)

        def draw(self, screen):
            pygame.draw.circle(screen, (0,0,255), (self.pos), 15, 0)

        def move(self, player1,player2):
            global b_speed
            x_loc = self.pos[0]
            y_loc = self.pos[1]
            x_paddle = player1.pos[0]
            y_paddle = player1.pos[1]
            x_paddle_ai = player2.pos[0]
            y_paddle_ai = player2.pos[1]

            if y_loc >= y_paddle and y_loc <= y_paddle + 100:
                if x_loc >= x_paddle and x_loc <= x_paddle + 35:

                    self.h_dir = 'right'
            if y_loc + 15 >= y_paddle_ai and y_loc + 15 <= y_paddle_ai + 100:
                if x_loc + 15 >= x_paddle_ai and x_loc + 15 <= x_paddle_ai + 35:

                    self.h_dir = 'left'

            if y_loc <= 75+15:
                self.v_dir = 'down'

            if y_loc >= 475-15:

                self.v_dir = 'up'

            if self.h_dir == 'left':
                x_loc = x_loc - 1 * b_speed

            if self.h_dir == 'right':
                x_loc = x_loc + 1 * b_speed

            if self.v_dir == 'down':
                y_loc = y_loc + 1 * b_speed

            if self.v_dir == 'up':
                y_loc = y_loc - 1  * b_speed

            if x_loc <= 5:
                self.kill_flag = True
                player2.score = player2.score + 1

            if x_loc >= 635:
                self.kill_flag = True
                player1.score = player1.score + 1
            #print self.kill_flag

            self.pos = (x_loc, y_loc)
            self.draw(screen)




    def run(self):
        play = 'true'
        pause = False
        player1 = self.Paddle()
        player2 = self.CompPaddle()
        playingField = self.Field()
        self.clear(screen)
        playingField.draw(screen)
        player1.draw(screen)
        player2.draw(screen)
        self.newGame(screen)
        while play == 'true':
            for event in pygame.event.get():
                if event.type == pygame.KEYUP and event.dict['key'] == pygame.K_SPACE:
                    pause = not pause
            key_pressed = pygame.key.get_pressed()
            if not pause:
                self.clear(screen)
                playingField.draw(screen)
                player1.draw(screen)
                player2.move(screen, self.circles)
                player2.draw(screen)

                for circle in self.circles:
                    if circle.kill_flag == True:
                        self.circles.remove(circle)
                        if len(self.circles) == 0:
                            self.newGame(screen)
                    else:
                        circle.move(player1, player2)
                if key_pressed[K_UP]:
                    player1.moveUp(screen)
                if key_pressed[K_DOWN]:
                    player1.moveDown(screen)

            pygame.key.set_repeat()
            if key_pressed[K_ESCAPE]:
                play = 'false'

            pygame.display.update()
            pygame.display.flip()
            pygame.time.delay(10)


game().run()
